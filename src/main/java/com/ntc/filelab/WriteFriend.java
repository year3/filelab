package com.ntc.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author L4ZY
 */
public class WriteFriend {

    public static void main(String[] args) {
        ArrayList<Friend> friendlist =new ArrayList<>();
        friendlist.add(new Friend("ntc", 20, "0986031686"));
        friendlist.add(new Friend("fix", 19, "0981216005"));
        friendlist.add(new Friend("kangsom", 18, "0948422999"));

        FileOutputStream fos = null;
        try {

            //friends.dat
            File file = new File("List_Friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
