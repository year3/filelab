
package com.ntc.filelab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author L4ZY
 */
public class ReadFriend {
    public static void main(String[] args) {
        ArrayList<Friend> friendlist =null;
        FileInputStream fis = null;
        try {

            File file =new File ("List_Friends.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois =new ObjectInputStream(fis);
            friendlist = (ArrayList<Friend>) ois.readObject();
            for(Friend friend: friendlist) 
            {
                System.out.println(friend);
            }
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        
        
    }
    
}
